package com.example.synchronizetemplate;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.synchronizetemplate.databinding.ActivityMainBinding;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class MainActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private Session mySession;
    private Thread sessionThread;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((binding = ActivityMainBinding.inflate(getLayoutInflater())).getRoot());


        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build());

        ActivityResultLauncher<Intent> signInClient = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    System.out.println("signinClient");
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        System.out.println("111111");
                        try {
                            System.out.println("2222222");
                            firebaseAuthWithGoogle(GoogleSignIn.getSignedInAccountFromIntent(result.getData()).getResult(ApiException.class));
                        } catch (ApiException e) {
                            System.out.println("333333");
                        }
                    }
                    System.out.println("6666666");
                });

        binding.gSingIn.setOnClickListener(view1 -> {
            System.out.println("click gnsingin");
            signInClient.launch(googleSignInClient.getSignInIntent());
        });

        binding.playBtn.setOnClickListener(click -> {
            play();
        });

        binding.pauseBtn.setOnClickListener(click2 ->{
            pause();
        });
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {

        if(account == null) return;
        System.out.println("546546465");
        FirebaseAuth.getInstance().signInWithCredential(GoogleAuthProvider.getCredential(account.getIdToken(), null))
                .addOnCompleteListener(this, task -> {

                    if (task.isSuccessful()) {
                        Session session = new Session();
                        session.ownerName = FirebaseAuth.getInstance().getCurrentUser().getEmail();
                        session.hour = 12;
                        session.minute = 10;
                        session.second = 50;
                        session.isPlaying = false;
                        FirebaseFirestore.getInstance().collection("Minutes").document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .set(session)
                                .addOnSuccessListener(task2 -> {
                                    Toast.makeText(this,"Collection published", Toast.LENGTH_LONG).show();
                                });
                    } else {

                    }
                });
    }

    private void joinSession(){
        
    }

    private void updateSession(){

    }

    private void play(){
        if (mySession == null) return;
        sessionThread = new Thread(() -> {

        });
        sessionThread.start();
    }

    private void pause(){
        if(sessionThread != null){
            sessionThread.interrupt();
        }
    }


}