package com.musiquitaapp.FirebaseControllers;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.musiquitaapp.models.PlayListFirebase;
import com.musiquitaapp.models.YouTubeVideo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

class PlaylistReturn{
    public List<PlayListFirebase> playlists;
}

public class PlaylistController {

    public PlaylistReturn myResult;
    private PlayListFirebase myPlaylist;
    private FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

    public void createPlaylist(List<YouTubeVideo> songs){
        myPlaylist = new PlayListFirebase(currentUser.getEmail(), currentUser.getUid(),
                "Demo", "1", "", songs);

        FirebaseFirestore.getInstance()
                .collection("Playlists")
                .document(myPlaylist.playlistID)
                .set(myPlaylist);
    }

    public void deletePlaylist(String playlistID){
        FirebaseFirestore.getInstance().collection("Playlists")
                .document(playlistID)
                .delete();
    }

    public List<PlayListFirebase> getPlaylistsByTitle(String playlistTitle){
        List<PlayListFirebase> result = new ArrayList<>();
        FirebaseFirestore.getInstance()
                .collection("Playlists")
                .whereEqualTo("playlistTitle", playlistTitle)
                .addSnapshotListener((value, error) -> {
                     for(DocumentSnapshot doc: value.getDocuments()){
                        result.add(doc.toObject(PlayListFirebase.class));
                     }
                });
        return result;
    }

    public List<PlayListFirebase> getAllUserPlaylistsByUsername(String username){
        List<PlayListFirebase> result = new ArrayList<>();
        FirebaseFirestore.getInstance()
                .collection("Playlists")
                .whereEqualTo("authorName", username)
                .addSnapshotListener((value, error) -> {
                    for(DocumentSnapshot doc: value.getDocuments()){
                        result.add(doc.toObject(PlayListFirebase.class));
                    }
                });
        return result;
    }

    private void getAllUserPlaylistsByUUID(String uuid){
       
        FirebaseFirestore.getInstance()
                .collection("Playlists")
                .whereEqualTo("authorID", uuid)
                .get()
                .addOnCompleteListener(task -> {

                })
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<PlayListFirebase> result = new ArrayList<>();
                    myResult = new PlaylistReturn();
                        for(DocumentSnapshot doc: queryDocumentSnapshots.getDocuments()){
                            //result.add(doc.getReference().get().getResult().toObject(PlayListFirebase.class));
                            result.add(doc.toObject(PlayListFirebase.class));
                        }
                    myResult.playlists = result;
                });
    }

    public DocumentReference getPlaylistReferenceByID(String playlistID){
        List<DocumentReference> result = new ArrayList<>();
        FirebaseFirestore.getInstance()
                .collection("Playlists")
                .whereEqualTo("playlistID", playlistID)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        result.add(value.getDocuments().get(0).getReference());
                    }
                });
        return result.get(0);
    }

    public void addSongToPlaylist(YouTubeVideo song, String playlistID){
        FirebaseFirestore.getInstance()
                .collection("Playlists")
                .whereEqualTo("playlistID", playlistID)
                .addSnapshotListener((value, error) -> {
                    value.getDocuments().get(0).getReference()
                            .get()
                            .addOnSuccessListener(documentSnapshot ->
                                    myPlaylist = documentSnapshot.toObject(PlayListFirebase.class));
                    myPlaylist.songs.add(song);
                    value.getDocuments().get(0).getReference()
                            .set(myPlaylist);
                });
    }

    public void removeSong(YouTubeVideo song, String playlistID){
        FirebaseFirestore.getInstance()
                .collection("Playlists")
                .whereEqualTo("playlistID", playlistID)
                .addSnapshotListener((value, error) -> {
                    value.getDocuments().get(0).getReference()
                            .get()
                            .addOnSuccessListener(documentSnapshot ->
                                    myPlaylist = documentSnapshot.toObject(PlayListFirebase.class));
                    myPlaylist.songs.remove(song);
                    value.getDocuments().get(0).getReference()
                            .set(myPlaylist);
                });
    }
}
