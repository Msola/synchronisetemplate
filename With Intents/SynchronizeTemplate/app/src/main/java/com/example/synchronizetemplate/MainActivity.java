package com.example.synchronizetemplate;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.synchronizetemplate.databinding.ActivityMainBinding;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;

public class MainActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private Session mySession;
    private Thread sessionThread;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((binding = ActivityMainBinding.inflate(getLayoutInflater())).getRoot());

        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build());

        ActivityResultLauncher<Intent> signInClient = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    System.out.println("signinClient");
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        System.out.println("111111");
                        System.out.println(result.getResultCode());
                        try {
                            System.out.println("2222222");
                            createSession(GoogleSignIn.getSignedInAccountFromIntent(result.getData()).getResult(ApiException.class));
                        } catch (ApiException e) {
                            System.out.println("333333");
                        }
                    }
                    System.out.println("6666666");
                });

        binding.gSingIn.setOnClickListener(view1 -> {
            System.out.println("click gnsingin");
            signInClient.launch(googleSignInClient.getSignInIntent());
        });

        binding.playBtn.setOnClickListener(click -> {
            play();
        });

        binding.pauseBtn.setOnClickListener(click2 ->{
            pause();
        });

    }

    private void joinSession(){

    }

    private void updateSession(){
        FirebaseFirestore.getInstance().collection("Minutes")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .set(mySession);
    }

    private void play(){
        if (mySession == null) return;
        mySession.isPlaying = true;
        sessionThread = new Thread(() -> {
            try {
                Thread.sleep(1000);
                mySession.second++;
                updateSession();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        sessionThread.start();
    }

    private void pause(){
        if(sessionThread != null){
            mySession.isPlaying = false;
            sessionThread.interrupt();
        }
    }

    private void createSession(GoogleSignInAccount account) {
        if(account == null) return;
        System.out.println("546546465");
        FirebaseAuth.getInstance().signInWithCredential(GoogleAuthProvider.getCredential(account.getIdToken(), null))
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        mySession = new Session();
                        mySession.ownerName = FirebaseAuth.getInstance().getCurrentUser().getEmail();
                        mySession.hour = 00;
                        mySession.minute = 00;
                        mySession.second = 00;
                        mySession.isPlaying = false;
                        FirebaseFirestore.getInstance().collection("Minutes")
                                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .set(mySession)
                                .addOnSuccessListener(task2 -> {
                                    Toast.makeText(this,"Collection published", Toast.LENGTH_LONG).show();
                                });
                    } else {

                    }
                });
    }




}