package com.example.synchronizetemplate;

public enum PlayerDirections {
    BACKWARDS,
    FORWARDS;
}
