package com.example.synchronizetemplate.models;

import com.example.synchronizetemplate.Song;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlayListFirebase {
    public String authorName;
    public String authorID;
    public String playlistID;
    public String playlistDescrip;
    public String playlistTitle;
    public String thumbnail;
    public List<YouTubeVideo> songs;

    public PlayListFirebase() {
    }

    public PlayListFirebase(String authorName, String authorID, String playlistDescrip,
                            String playlistTitle, String thumbnail, List<YouTubeVideo> songs) {
        this.authorName = authorName;
        this.authorID = authorID;
        this.playlistID = UUID.randomUUID().toString();
        this.playlistDescrip = playlistDescrip;
        this.playlistTitle = playlistTitle;
        this.thumbnail = thumbnail;
        this.songs = songs;
    }
}
