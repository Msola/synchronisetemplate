package com.example.synchronizetemplate;

import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.synchronizetemplate.databinding.FragmentPantall1Binding;
import com.example.synchronizetemplate.models.PlayListFirebase;
import com.example.synchronizetemplate.models.YouTubeVideo;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlaylistController {

    private static DocumentReference db;
    private FirebaseAuth mAuth;
    private PlayListFirebase myPlaylist;
    private FirebaseUser currentUser;
    private FragmentActivity myActivity;

    public void setCurrentUser(FirebaseUser currentUser) {
        this.currentUser = currentUser;
    }

    /*public void CreatePrivatePlaylist(List<Song> songs){
        myPlaylist = new PlayListFirebase(currentUser.getEmail(), UUID.randomUUID().toString(),
                "Demo", "1", "", songs);

        FirebaseFirestore.getInstance()
                .collection("Playlists")
                .document(currentUser.getUid())
                .collection("UserPlaylists")
                .document(myPlaylist.identifier)
                .set(myPlaylist);
    }*/

    public void CreatePlaylist(String title, String description, String thumbnailUrl){
        List<YouTubeVideo> songs = new ArrayList<>();
        myPlaylist = new PlayListFirebase(currentUser.getEmail(), currentUser.getUid(),
                description, title, thumbnailUrl, songs);

        FirebaseFirestore.getInstance()
                .collection("Playlists")
                .document(myPlaylist.playlistID)
                .set(myPlaylist);
    }

    public List<PlayListFirebase> GetPlaylistsByTitle(String playlistTitle){
        List<PlayListFirebase> result = new ArrayList<>();
        FirebaseFirestore.getInstance()
                .collection("Playlists")
                .whereEqualTo("playlistTitle", playlistTitle)
                .addSnapshotListener((value, error) -> {
                     for(DocumentSnapshot doc: value.getDocuments()){
                        result.add(doc.toObject(PlayListFirebase.class));
                     }
                });
        return result;
    }

    public List<PlayListFirebase> GetAllUserPlaylistsByUsername(String username){
        List<PlayListFirebase> result = new ArrayList<>();
        FirebaseFirestore.getInstance()
                .collection("Playlists")
                .whereEqualTo("authorName", username)
                .addSnapshotListener((value, error) -> {
                    for(DocumentSnapshot doc: value.getDocuments()){
                        result.add(doc.toObject(PlayListFirebase.class));
                    }
                });
        return result;
    }

    public List<PlayListFirebase> GetAllUserPlaylistsByUUID(String uuid){
        List<PlayListFirebase> result = new ArrayList<>();
        FirebaseFirestore.getInstance()
                .collection("Playlists")
                .whereEqualTo("authorID", uuid)
                .addSnapshotListener((value, error) -> {
                    for(DocumentSnapshot doc: value.getDocuments()){
                        result.add(doc.toObject(PlayListFirebase.class));
                    }
                });
        return result;
    }

    public void AddSongToPlaylist(YouTubeVideo song, String playlistID){
        FirebaseFirestore.getInstance()
                .collection("Playlists")
                .whereEqualTo("playlistID", playlistID)
                .addSnapshotListener((value, error) -> {
                    value.getDocuments().get(0).getReference()
                            .get()
                            .addOnSuccessListener(documentSnapshot ->
                                    myPlaylist = documentSnapshot.toObject(PlayListFirebase.class));
                    myPlaylist.songs.add(song);
                    value.getDocuments().get(0).getReference()
                            .set(myPlaylist);
                });
    }

    public void RemoveSong(Song song, PlayListFirebase playListFirebase){

    }
}
