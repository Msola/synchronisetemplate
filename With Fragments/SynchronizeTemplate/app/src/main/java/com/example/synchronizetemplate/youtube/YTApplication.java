package com.example.synchronizetemplate.youtube;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.example.synchronizetemplate.models.YouTubeVideo;

import java.util.ArrayList;

;

public class YTApplication extends Application {

    private static Context mContext;
    private static ArrayList<YouTubeVideo> mediaItems;
    private static YTApplication ourInstance = new YTApplication();
    private static MutableLiveData<Integer> pos = new MutableLiveData<>();
    public void onCreate() {
        super.onCreate();
        mediaItems = new ArrayList<>();
        mContext = getApplicationContext();
        pos.setValue(0);
    }

    public static MutableLiveData<Integer> getPos() { return pos; }

    public static void setPos(MutableLiveData<Integer> pos) { YTApplication.pos = pos; }

    public static YTApplication getOurInstance() { return ourInstance; }
    public static ArrayList<YouTubeVideo> getMediaItems(){ return mediaItems;}
    public static Context getAppContext() {
        return mContext;
    }

}