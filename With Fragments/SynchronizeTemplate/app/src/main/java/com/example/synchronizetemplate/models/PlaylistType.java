package com.example.synchronizetemplate.models;

public enum PlaylistType {
    ALBUM, PLAYLIST, FAVORITES, UPLOADS, HISTORY
}
