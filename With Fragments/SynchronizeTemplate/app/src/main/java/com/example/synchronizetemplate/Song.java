package com.example.synchronizetemplate;

import com.google.android.exoplayer2.Timeline;

import java.util.HashMap;

public class Song {
    public String youtubeID;

    public String title;
    public String thumbnailURL;
    public String duration;
    public String viewCount;
    public String artistName;
    public String albumName;

    //Firebase Constructor
    public Song() {
    }

    public Song(String youtubeID, String title, String thumbnailURL, String duration, String viewCount, String artistName, String albumName) {
        this.youtubeID = youtubeID;
        this.title = title;
        this.thumbnailURL = thumbnailURL;
        this.duration = duration;
        this.viewCount = viewCount;
        this.artistName = artistName;
        this.albumName = albumName;
    }

    //private HashMap<String, Boolean> likes = new HashMap<>();

}
