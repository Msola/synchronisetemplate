package com.example.synchronizetemplate;

//https://stackoverflow.com/questions/44229111/access-youtube-channel-id-once-signed-in-though-firebase-googleauthprovider

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.synchronizetemplate.databinding.FragmentPantall1Binding;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

//import com.google.api.services.samples.youtube.cmdline.Auth;
import com.google.api.services.youtube.YouTube;


public class Pantall1Fragment extends Fragment {

    private static YouTube youTube;

    private FragmentPantall1Binding binding;
    private NavController navController;
    private ComunicatorSingleton comunicatorSingleton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return (binding = FragmentPantall1Binding.inflate(inflater, container, false)).getRoot();    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(requireView());

        comunicatorSingleton = ComunicatorSingleton.getInstance();
        comunicatorSingleton.controller.setBinding(binding);
        comunicatorSingleton.controller.setComunicator(comunicatorSingleton);
        comunicatorSingleton.controller.setMyActivity(getActivity());
        comunicatorSingleton.service.setComunicator(comunicatorSingleton);

        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(requireActivity(), new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build());

        ActivityResultLauncher<Intent> signInClient = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                try {
                            firebaseAuthWithGoogle(GoogleSignIn.getSignedInAccountFromIntent(
                                    result.getData()).getResult(ApiException.class
                            ));
                } catch (ApiException e) {
                    System.out.println(e.toString());
                }
            }
        });

        binding.gSingIn.setOnClickListener(view1 -> {
            signInClient.launch(googleSignInClient.getSignInIntent());
            //comunicatorSingleton.controller.setCurrentUser(FirebaseAuth.getInstance().getCurrentUser());

        });

        binding.createSessionBtn.setOnClickListener(myclick ->  {
            comunicatorSingleton.controller.createSession();
            comunicatorSingleton.controller.createAudioService(requireActivity());
        });

        binding.playBtn.setOnClickListener(click -> {
            comunicatorSingleton.controller.play();
        });

        binding.pauseBtn.setOnClickListener(click2 ->{
            comunicatorSingleton.controller.pause(false);
        });
        binding.stopBtn.setOnClickListener(click3 ->{
            comunicatorSingleton.controller.deleteSession();
        });
        binding.joinBtn.setOnClickListener(click4 ->{
            comunicatorSingleton.controller.joinSession("msolabsch@gmail.com");
            comunicatorSingleton.controller.createAudioService(requireActivity());
        });
        binding.readyBtn.setOnClickListener(click5 -> {
            //comunicatorSingleton.controller.videoIsReadyCall();
        });
        binding.backwardBtn.setOnClickListener(click6 ->{
            comunicatorSingleton.controller.songChange(PlayerDirections.BACKWARDS);
        });
        binding.forwardBtn.setOnClickListener(click7 ->{
            comunicatorSingleton.controller.songChange(PlayerDirections.FORWARDS);
        });


    }
    public void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        if(account == null) return;

        FirebaseAuth.getInstance().signInWithCredential(
            GoogleAuthProvider.getCredential(account.getIdToken(), null)
        );
    }
}