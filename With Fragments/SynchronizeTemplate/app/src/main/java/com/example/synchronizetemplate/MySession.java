package com.example.synchronizetemplate;

import com.google.android.exoplayer2.Timeline;

import java.util.List;

public class MySession {
    public String sessionName;
    public String ownerName;
    public int hour;
    public int minute;
    public int second;
    public boolean isPlaying;
    public boolean recentlyJoined;
    public int currentSongIndex;
    public Song currentSong;
    public List<Song> songsInQueue;
    public int connectedUsers;
    public List<Boolean> isUserReady;

    //Firebase Controller


    public MySession() {
    }

    public MySession(String sessionName, String ownerName, int hour, int minute, int second,
                     boolean isPlaying, boolean recentlyJoined, int currentSongIndex, Song currentSong,
                     List<Song> songsInQueue, int connectedUsers)
    {
        this.sessionName = sessionName;
        this.ownerName = ownerName;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.isPlaying = isPlaying;
        this.recentlyJoined = recentlyJoined;
        this.currentSongIndex = currentSongIndex;
        this.currentSong = currentSong;
        this.songsInQueue = songsInQueue;
        this.connectedUsers = connectedUsers;
    }
}
